import { defineConfig } from 'astro/config';
import solidJs from "@astrojs/solid-js";

import Icons from 'unplugin-icons/vite'
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  output: "hybrid",
  integrations: [solidJs(), tailwind()], 
  vite: {
      plugins: [
          Icons({
              compiler: 'solid'
          })
      ]
  }
});