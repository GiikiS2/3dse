import { atom } from 'nanostores';

export const isBusy = atom(false);
export const results = atom([]);
export const query = atom("");

export const filter = atom(["free", "for sale"]);
export const sort = atom("best match");

