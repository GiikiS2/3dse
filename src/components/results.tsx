import { isBusy, results, filter, sort } from '../Store';
import { useStore } from '@nanostores/solid';
import Card from './card';
import { navigate } from 'astro:transitions/client';
import { onMount } from 'solid-js';
import { createSignal } from 'solid-js';

import Accordion from '@corvu/accordion'

//@ts-ignore
import Sort from "~icons/ph/sort-ascending-duotone";
//@ts-ignore
import Filter from "~icons/solar/filter-bold-duotone";
//@ts-ignore
// import Sort from "~icons/ph/sort-ascending-duotone"; 
// //@ts-ignore
// import Filter from "~icons/solar/filter-bold-duotone"; 

const Results = () => {
  const [query, setQuery] = createSignal<string>("");
  const $isBusy = useStore(isBusy);
  const $results = useStore(results);
  const $sort = useStore(sort);
  const $filter = useStore(filter);

  var noanim: HTMLDivElement | ((el: HTMLDivElement) => void) | undefined;

  onMount(async () => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    const queryParam = params.query;
    setQuery(queryParam);

    if (!queryParam || queryParam === undefined) {
      navigate('/');
      return;
    }

    document.title = queryParam+" | 3dse"

    setTimeout(function(){
      (noanim as HTMLDivElement).classList.remove('preload')
  },500);

    isBusy.set(true);
    
    try {
      const response = await fetch(`http://localhost:4321/api/q/${queryParam}`);
      const data = await response.json();
      results.set(data)
      // if (data.length > 0) 
      isBusy.set(!$isBusy());
    } catch (error) {
      console.error('Error fetching search results:', error);
    } finally {
      isBusy.set(false);
    }
  });

  //@ts-ignore
  const sortByPrice = (a, b) => a.price.cents - b.price.cents;
  //@ts-ignore
  const sortByDate = (a, b) => new Date(b.date) - new Date(a.date);
  //@ts-ignore
  const sortByRelevance = (a, b) => b.relevance - a.relevance;


  const getSortedData0 = () => {
    switch ($sort()) {
      case 'by price':
        return [...$results()].sort(sortByPrice);
      case 'latest':
        return [...$results()].sort(sortByDate);
      case 'best match':
        // return [...$results()].sort(sortByRelevance);
        return $results();
      default:
        return $results();
    }
  }

  const getSortedData = () => {
    if ($filter().includes('free') && $filter().includes('for sale')) {
      return getSortedData0();
    } else if ($filter().includes('free')) {
      return getSortedData0().filter((item: any) => item.price.cents === 0);
    } else if ($filter().includes('for sale')) {
      return getSortedData0().filter((item: any) => item.price.cents > 0);
    } else {
      return getSortedData0(); // No filter criteria matched
    }
  };

  const [bookmarks, setBookmarks] = createSignal<string[]>([]);

  onMount(() => {
    const storedBookmarks = localStorage.getItem("bookmarks");
    setBookmarks(storedBookmarks ? JSON.parse(storedBookmarks) : []);
  });

  const addBookmark = (url: string) => {
    const newBookmarks = [...bookmarks(), url];
    setBookmarks(newBookmarks);
    localStorage.setItem("bookmarks", JSON.stringify(newBookmarks));
  };

  const removeBookmark = (url: string) => {
    const newBookmarks = bookmarks().filter((bookmark) => bookmark !== url);
    setBookmarks(newBookmarks);
    localStorage.setItem("bookmarks", JSON.stringify(newBookmarks));
  };

  const toggleBookmark = (url: string) => {

    console.log(url)

    let updatedBookmarks;

    if (bookmarks().some((item) => item === url)) {
      updatedBookmarks = bookmarks().filter((item) => item !== url);
    } else {
      updatedBookmarks = [...bookmarks(), url];
    }
    setBookmarks(updatedBookmarks);
    localStorage.setItem("bookmarks", JSON.stringify(updatedBookmarks));

    console.log(updatedBookmarks)

  };



  return (
    <div class="m-3 rounded-lg rounded-l-xl bg-green bg-opacity-90 flex">
      <div ref={noanim} class="flex rounded-l-lg flex-col bg-base w-96 text-text p-3 font3 preload">

        <Accordion collapseBehavior="hide" multiple initialValue={["show", "sort"]}>
          <Accordion.Item value="show">
            <h2>
              <Accordion.Trigger class="w-full text-left font-medium transition-all duration-100 hover:bg-corvu-200 focus-visible:bg-corvu-200 focus-visible:outline-none">
                <summary
                  class="flex cursor-pointer items-center justify-between gap-2 transition"
                >
                  <span class="pr-3 gap-1 font-bold text-lg flex items-center">
                    <Filter></Filter> Show
                  </span>

                  <span class="transition group-open:-rotate-180">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="h-4 w-4"
                    >
                      <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                    </svg>
                  </span>
                </summary>
              </Accordion.Trigger>
            </h2>
            <Accordion.Content class="overflow-hidden corvu-expanded:animate-expand corvu-collapsed:animate-collapse">
              {["free", "for sale"].map((el) => (

                <div class="cursor-pointer px-2 p-1 gap-2 flex items-center w-full leading-tight rounded-lg outline-none text-start hover:bg-surface0 hover:bg-opacity-80 hover:text-subtext1 focus:bg-surface0 focus:bg-opacity-80 focus:text-subtext1 active:bg-surface0 active:bg-opacity-80 active:text-subtext1">
                  <input class="ui-checkbox" id={el} type="checkbox"
                    checked={$filter().includes(el)}

                    onclick={(ell) => {

                      const index = $filter().indexOf(el);

                      if (index > -1) {
                        // Item is in the list, so remove it
                        // $filter().splice(index, 1);
                        filter.set($filter().filter(i => i !== el));
                      } else {
                        // Item is not in the list, so add it
                        // $filter().push(el);
                        filter.set([...$filter(), el]);
                      }

                      // filter.value?.push(el)
                      // filter.value?.splice(index, 1);
                      // filter.set(el)
                    }} />
                  <label class="cursor-pointer w-full" for={el}>{el}</label>
                </div>
              ))}
            </Accordion.Content>
          </Accordion.Item><Accordion.Item value="sort">
            <h2>
              <Accordion.Trigger class="w-full text-left font-medium transition-all duration-100 hover:bg-corvu-200 focus-visible:bg-corvu-200 focus-visible:outline-none">
                <summary
                  class="flex cursor-pointer items-center justify-between gap-2 transition"
                >
                  <span class="pr-3 gap-1 font-bold text-lg flex items-center">
                    <Sort></Sort> Sort
                  </span>

                  <span class="transition group-open:-rotate-180">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="h-4 w-4"
                    >
                      <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                    </svg>
                  </span>
                </summary>
              </Accordion.Trigger>
            </h2>
            <Accordion.Content class="overflow-hidden corvu-expanded:animate-expand corvu-collapsed:animate-collapse">
              {["by price", "latest", "best match"].map((el) => (

                <div class="cursor-pointer px-2 p-1 gap-2 flex items-center w-full leading-tight rounded-lg outline-none text-start hover:bg-surface0 hover:bg-opacity-80 hover:text-subtext1 focus:bg-surface0 focus:bg-opacity-80 focus:text-subtext1 active:bg-surface0 active:bg-opacity-80 active:text-subtext1">
                  <input
                    type="radio"
                    name="radio"
                    class="ui-checkbox" id={el}
                    checked={$sort() === el}
                    onclick={(ell) => {
                      sort.set(el)
                    }}
                  />
                  <label class="cursor-pointer w-full" for={el}>{el}</label>
                </div>
              ))}
            </Accordion.Content>
          </Accordion.Item>
        </Accordion>


      </div>
      <div class="container w-full p-4">
        {getSortedData().map((model: any) => (
          <Card model={model} className="" useBookmarks={{bookmarks, toggleBookmark}}>
            {/* toggleBookmark={toggleBookmark} bookmarked={bookmarked} */}
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Results;
