import { createSignal, onCleanup, onMount } from 'solid-js';
import { createStore } from 'solid-js/store';
// import Bookmark1 from "https://icons.church/solar/bookmark-bold";
// import Bookmark2 from "https://icons.church/solar/bookmark-bold-duotone";

import { Icon } from "@iconify-icon/solid";

// import { Model } from "../global.tsx";
export interface Model {
  name: string;
  url: string;
  img: string;
  price?: {
    cents: number;
  };
}

const Card = (props: { model: Model, className?: string, useBookmarks: { bookmarks: any, toggleBookmark: any } }) => {
  const [isOverflowing, setIsOverflowing] = createSignal(false);
  let name: HTMLParagraphElement | ((el: HTMLParagraphElement) => void) | undefined

  const checkOverflow = () => {
    if (name) {
      //@ts-ignore
      setIsOverflowing(name.scrollWidth > name.clientWidth);
    }
  };

  onMount(() => {
    checkOverflow();
  });

  return (
    <div class={"w-64 h-fit bg-surface0 rounded-xl overflow-hidden text-text text-center outline-surface1 outline outline-2 " + props.className}>
      <div class="flex flex-col h-full w-full">
        <div class="w-full h-2/3 bg-overlay2">
          <a href={props.model.url} target="_blank" class="w-full h-full">
          {/* <img src={"/api/img?isrc="+props.model.img} alt={props.model.name} class="w-full border-b border-surface1 h-full" /> */}
            <img src={props.model.img} alt={props.model.name} class="w-full border-b border-surface1 h-full" />
          </a>
        </div>
        <a href={props.model.url} target="_blank" class="h-1/6 mp w-full">
          {isOverflowing() ? (
            <p ref={name} class="py-2 text-nowrap overflow-hidden marqueee">{props.model.name}</p>
          ) : (
            <p ref={name} class="py-2 text-nowrap overflow-hidden">{props.model.name}</p>
          )}
        </a>
        <div class="w-full h-1/6 bg-mantle">
          <div class="h-full flex items-center justify-center">
            <div class="w-full h-full border-r border-surface1 flex justify-center items-center">
              <button>
                <label class="ui-bookmark">
                  <input type="checkbox" checked={props.useBookmarks.bookmarks().some((item: any) => item === props.model.url) ? true : false} onclick={
                (el) => {
                  props.useBookmarks.toggleBookmark(props.model.url)
                }
              }/>
                  <div class="bookmark">
                    <Icon icon={props.useBookmarks.bookmarks().some((item: any) => item === props.model.url) ? "solar:bookmark-bold" : "solar:bookmark-bold-duotone"} inline />
                  </div>
                </label>
              </button>
            </div>
            <div class="w-full h-full py-2 flex justify-center items-center">
              {props.model.price ? (
                props.model.price.cents === 0 ? (
                  <span class="bg-green rounded-lg p-2 py-0 text-[#1e1e2e]">Free</span>
                ) : (
                  `US$ ${(props.model.price.cents / 100).toFixed(2)}`
                )
              ) : (
                <div></div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
