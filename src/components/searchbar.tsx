// import { createSignal, Show } from "solid-js";
import { useStore } from '@nanostores/solid';
import { isBusy } from '../Store';
import { Show, createMemo } from "solid-js";
//@ts-ignore
import Dots from "~icons/solar/menu-dots-bold-duotone"; 
//@ts-ignore
import Load from "~icons/mingcute/loading-3-fill"; 

import { navigate } from 'astro:transitions/client';



const SearchBar = () => {
  const $isBusy = useStore(isBusy);

  let myElement!: HTMLInputElement;
  return (

    <div class="w-fit flex gap-2 text-base font3">
      <button class="p-2 rounded-xl bg-flamingo text-surface1 outline hover:outline-red outline-transparent outline-2 text-2xl">
        {/* <Icon icon="solar:menu-dots-bold-duotone" rotate="90deg" inline></Icon> */}
        <Dots width={24} height={24} class="rotate-90"></Dots>
        {/* <CheckIcon width={24} height={24}></CheckIcon> */}
      </button>
      <input
        ref={myElement}
        class="p-2 rounded-xl bg-white outline-none"
        type="text"
        placeholder="Query.."
        // value={query}
        // onInput={handleInput}
        // onKeyPress={(e)=>{e.key==="Enter" ? location.href = "/q/" + (ref.current as unknown as HTMLInputElement).value : null}}
        onKeyPress={(e) => {
          if (e.key === "Enter") {
              isBusy.set(!$isBusy());
              // Astro.redirect("/q/" + myElement.value);
              // location.href = "/q/" + myElement.value;
              navigate("/q?query=" + myElement.value)
          }
      }}
      />
      <button
        class="p-2 rounded-xl bg-flamingo text-surface1 outline hover:outline-red outline-transparent outline-2"
        onClick={()=>{
          isBusy.set(!$isBusy());
          navigate("/q?query=" + myElement.value)
        }}
          // location.href = "/q/" + myElement.value}}
           // onClick={(e) => console.log(myElement)}
        //Astro.redirect('/login');
      >
        <span class="flex items-end justify-between w-full">
          {/* {props.state.value === 'busy' ? ( <Load className="animate-spin" width={20} height={20}></Load>) : null} search */}
          <Load
            className="animate-spin"
            width={$isBusy() ? 20 : 0}
            height={20}
          >
          </Load>
          
          search
        </span>
      </button>
    </div>
  );
};

export default SearchBar;
