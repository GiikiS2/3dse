// Outputs: /builtwith.json
export const prerender = false;

import axios from "axios"

export interface Model {
  name: string;
  url: string;
  img: string;
  price?: {
    cents: number;
  };
}

async function scrape3DModels(query: string) {

  const links = [
    `https://www.thingiverse.com/search?q=${query}`,
    `https://cults3d.com/en/search?q=${query}`
  ]
  try {
    const models : Model[] = [];

    for (const link of links) {

      const response = await axios.get(link);
      const html = response.data;

      if (link.includes("thingiverse")) {

        html.forEach((element: any) => {
          // console.log(element)
          var name = element.name
          var url = `https://www.thingiverse.com/thing:${element.id}`
          var img = element.thumbnail
          models.push({ name, url, img, price:{"cents":0}  });
        });

      }
        else {

          const USERNAME : string = import.meta.env.c_username || "";
          const PASSWORD : string = import.meta.env.c_password || "";
          const GRAPHQL_ENDPOINT = 'https://cults3d.com/graphql';

          try {
            const response = await axios.post(GRAPHQL_ENDPOINT, {
              query: `
      query {
        creationsSearchBatch(query: "${query}") {
          total
      results {
        name(locale: EN)
        url(locale: EN)
        price(currency: USD) {
          cents
        }
        illustrationImageUrl
        creator {
          nick
        }
      }
        }
      }`,
            }, {
              auth: {
                username: USERNAME,
                password: PASSWORD,
              },
              headers: {
                'Content-Type': 'application/json',
              },
            });
            // console.log(response.data)
            if (response.data.data.creationsSearchBatch) {
              // console.log('Search Results:', response.data.data.creationsSearchBatch.price);
              response.data.data.creationsSearchBatch.results.forEach((el: any) => {
                var name = el.name
                var url = el.url
                var img = el.illustrationImageUrl
                var price = el.price
                models.push({ name, url, img, price });
              })

            }
             else {
              return
            }
          } catch (error) {
            console.error('Error fetching search results:', error);
          }

        }

    }

    // console.log(models)

    if( models.length <= 0) return 404
    
    return models;
  } catch (error) {
    console.error('Error scraping 3D models:', error);
    return [];
  }
}

export async function GET({params}: {params:any}) {

// if(params.length < 0) return new Response(null, {
//   status: 422,
//   statusText: 'query missing.'
// });

  const response = await scrape3DModels(params.query);
  return new Response(JSON.stringify(response), { headers: {
    "Content-Type": "application/json"
  } });
  }