export const prerender = false;

import type { APIRoute } from 'astro';
import sharp from 'sharp';
import fetch from 'node-fetch';


// export async function GET({params}: {params:any}) {
export async function GET({ request }: any) {
// export const get: APIRoute = async ({ request }) => {
  const url = new URL(request.url);
  const imageUrl = url.searchParams.get('isrc');

  if (!imageUrl) {
    return new Response('Missing isrc parameter.', { status: 400 });
  }

  try {
    const response = await fetch(imageUrl);
    if (!response.ok) {
      return new Response('Failed to fetch image.', { status: 400 });
    }

    const buffer = await response.buffer();
    const optimizedBuffer = await sharp(buffer)
      .toFormat('avif')
      .avif({ quality: 50 }) // Adjust quality as needed
      .toBuffer();

    return new Response(optimizedBuffer, {
      headers: {
        'Content-Type': 'image/avif',
        'Content-Disposition': `attachment; filename="optimized.avif"`,
      },
    });
  } catch (error) {
    console.error(error);
    return new Response('Internal Server Error', { status: 500 });
  }
};
